package com.tute.moviecatalogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;


// Not using @EnableEurekaClient annotation here
// Now it's not a required annotation 
// adding "spring-cloud-starter-netflix-eureka-client" will enable it
@SpringBootApplication
public class MovieCatalogServiceApplication {

	@Bean
	@LoadBalanced // Does 2 things - 1.Client Side, Service Discovery 2.Load Balancing
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	
	public static void main(String[] args) {
		SpringApplication.run(MovieCatalogServiceApplication.class, args);
	}

}
