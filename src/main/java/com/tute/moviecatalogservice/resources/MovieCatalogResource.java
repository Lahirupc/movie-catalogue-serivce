package com.tute.moviecatalogservice.resources;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.tute.moviecatalogservice.models.CatalogItem;
import com.tute.moviecatalogservice.models.Movie;
import com.tute.moviecatalogservice.models.UserRating;

@RestController
@RequestMapping("/catalogue")
public class MovieCatalogResource {
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	@RequestMapping("/{userId}")
	public List<CatalogItem> getCatalog(String userId){
		
		// get all rated movie IDs
		UserRating ratings = restTemplate.getForObject("http://ratings-data-service:8083/ratingsdata/users/" + userId, UserRating.class);
		
		
		return ratings.getUserRating().stream().map(rating -> {
			// For each movie ID, call movie info service and get details
			Movie movie = restTemplate.getForObject("http://movie-info-service:8082/movies/" + rating.getMovieId(), Movie.class);
			// Put them all together
			return new  CatalogItem(movie.getName(), "Test", rating.getRating());
		
		})
		.collect(Collectors.toList());
		
	
	}

}

